import cowsay
import argparse

presets = 'bdgpstwy'

def custom_cowsay(args):
    # list mode
    if args.l:
        print(cowsay.list_cows())
        return
    
    # standard input message processing
    if args.message == None:
        lines = []
        while True:
            try:
                lines.append(input())
            except EOFError:
                break
        args.message = '\n'.join(lines)

    # cowfile option processing
    if '/' in args.f:
        with open(args.f, 'r', encoding='utf-8') as file:
            cow_name = None
            cow_file_lines = file.readlines()[1:-1]
            cow_file = ''
            for cow_file_line in cow_file_lines:
                cow_file += cow_file_line
    else:
        cow_name = args.f
        cow_file = None

    # preset processing
    preset = None
    for p in presets[::-1]:
        if args.__dict__[p]:
            preset=p
            break

    print(
        cowsay.cowsay(
        message=args.message,
        cow=cow_name,
        preset=preset,
        eyes=args.e[:2],
        tongue=args.T[:2],
        width=args.W,
        wrap_text=args.n,
        cowfile=cow_file
        )
    )
    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', type=str, default='oo', help='the appearance of the eyes of the cow')
    parser.add_argument('-f', type=str, default='default', help='a particular cow picture file ("cowfile") to use')
    parser.add_argument('-l', action='store_true', help='the available builtin cows')
    parser.add_argument('-n', action='store_false', help='whether the text should be wrapped in the bubble')
    parser.add_argument('-T', type=str, default='  ', help='the appearance of the eyes of the tongue')
    parser.add_argument('-W', type=int, default=40, help='the width of the text bubble')
    parser.add_argument('-b', action='store_true', help='Borg mode')
    parser.add_argument('-d', action='store_true', help='dead cow appearance')
    parser.add_argument('-g', action='store_true', help='greedy mode')
    parser.add_argument('-p', action='store_true', help='a state of paranoia to come over the cow mode')
    parser.add_argument('-s', action='store_true', help='thoroughly stoned mode')
    parser.add_argument('-t', action='store_true', help='tired cow appearance')
    parser.add_argument('-w', action='store_true', help='wired mode')
    parser.add_argument('-y', action='store_true', help='youthful appearance of the cow')
    parser.add_argument('message', action='store', type=str, nargs='?', default=None, help='a string to wrap in the text bubble')
    args = parser.parse_args()
    custom_cowsay(args)