import random
import argparse
import urllib.request

def ask(prompt: str, valid: list[str] = None) -> str:
    if valid == None:
        return input(prompt)
    else:
        while True:
            result = input(prompt)
            if result in valid:
                return result

def inform(format_string: str, bulls: int, cows: int) -> None:
    print(format_string.format(bulls, cows))

def bullscows(guess: str, secret: str) -> (int, int):
    bulls_num = 0
    
    if len(guess) != len(secret):
        print('Guess and secret must have the same length!')
        raise Exception
    
    cows_num = len(set(guess).intersection(set(secret)))

    for i in range(min(len(guess), len(secret))):
        if guess[i] == secret[i]:
            bulls_num += 1

    return bulls_num, cows_num

def gameplay(ask: callable, inform: callable, words: list[str]) -> int:
    attempts_num = 0
    secret = words[random.randint(0,len(words)-1)]

    while True:
        guess = ask('Enter the word: ', words)
        attempts_num += 1
        try:
            inform('Bulls: {}, Cows: {}', *bullscows(guess, secret))
        except Exception:
            continue
        if guess == secret:
            return attempts_num

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dict', type=str, help='dictionary file path or URL')
    parser.add_argument('length', nargs='?', type=int, default=5, help='word length')
    args = parser.parse_args()

    try:
        with open(args.dict, 'r', encoding='utf-8') as file:
            words = [word.strip() for word in file.readlines() if len(word.strip()) == args.length]
    except:
        try:
            with urllib.request.urlopen(args.dict) as file:
                words = [word.decode('utf-8').strip() for word in file.readlines() if len(word.decode('utf-8').strip()) == args.length]
        except:
            print(f'{args.dict} is not valid path or URL')
            exit()

    if len(words) == 0:
        print(f'There is no words with length {args.length} in {args.dict} dictionary!')
        exit()

    attempts_num = gameplay(ask, inform, words)
    print(f'Correct! Total attempts: {attempts_num}')