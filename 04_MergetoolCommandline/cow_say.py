import cowsay
import cmd, shlex

class CowCommandLine(cmd.Cmd):
    intro = 'Welcome in CowCommandLine!'
    prompt = '>>> '


    def do_cowsay(self, str):
        '''
        Generates an ASCII picture of a cow saying something provided by the user

        cowsay [-e eye_string] [-c cow] [-T tongue_string] message
        
        -e    Selects the appearance of the cow's eyes. It must be at least 2 characters long (default: 'oo')
        -c    A custom string representing a cow (default: 'default')
        -T    A custom tongue string. It must be at least 2 characters long (default: '  ')

        Multiword message must be in quotes.
        '''
        s = shlex.split(str)
        params = {'-e': 'oo', '-T': '  ', '-c': 'default'}
        for i in range(0, len(s)-1, 2):
            if s[i] in params:
                if s[i+1] in params:
                    print('Error: invalid syntax')
                    return
                params[s[i]] = s[i+1]
        if len(params['-e']) < 2:
            print('Error: the length of the eye string must be at least 2')
            return
        if len(params['-T']) < 2:
            print('Error: the length of the tongue string must be at least 2')
            return
        print(cowsay.cowsay(message=s[-1], cow=params['-c'], eyes=params['-e'][:2], tongue=params['-T'][:2]))


    def complete_cowsay(self, text, line, begidx, endidx):
        completions = {
            '-e': ['oo', 'oO', 'Oo', 'OO', 'o-', '-o', '--', 'O-', '-O'],
            '-T': ['uu', 'UU', 'vv', 'VV', 'db', '//'],
            '-c': cowsay.list_cows()
        }
        if text != '':
            return [s for s in completions[shlex.split(line)[-2]] if s.startswith(text)]
        else:
            return [s for s in completions[shlex.split(line)[-1]]]
        
    
    def do_cowthink(self, str):
        '''
        Generates an ASCII picture of a cow thinking about something provided by the user

        cowthink [-e eye_string] [-c cow] [-T tongue_string] message
        
        -e    Selects the appearance of the cow's eyes. It must be at least 2 characters long (default: 'oo')
        -c    A custom string representing a cow (default: 'default')
        -T    A custom tongue string. It must be 2 at least characters long (default: '  ')

        Multiword message must be in quotes.
        '''
        s = shlex.split(str)
        params = {'-e': 'oo', '-T': '  ', '-c': 'default'}
        for i in range(0, len(s)-1, 2):
            if s[i] in params:
                if s[i+1] in params:
                    print('Error: invalid syntax')
                    return
                params[s[i]] = s[i+1]
        if len(params['-e']) < 2:
            print('Error: the length of the eye string must be at least 2')
            return
        if len(params['-T']) < 2:
            print('Error: the length of the tongue string must be at least 2')
            return
        print(cowsay.cowthink(message=s[-1], cow=params['-c'], eyes=params['-e'][:2], tongue=params['-T'][:2]))


    def complete_cowthink(self, text, line, begidx, endidx):
        completions = {
            '-e': ['oo', 'oO', 'Oo', 'OO', 'o-', '-o', '--', 'O-', '-O'],
            '-T': ['uu', 'UU', 'vv', 'VV', 'db', '//'],
            '-c': cowsay.list_cows()
        }
        if text != '':
            return [s for s in completions[shlex.split(line)[-2]] if s.startswith(text)]
        else:
            return [s for s in completions[shlex.split(line)[-1]]]


    def do_list_cows(self, str):
        '''
        Lists all cow files in the given directory

        list_cows [dir]
        '''
        if len(str) == 0:
            print(cowsay.list_cows())
        else:
            print(cowsay.list_cows(shlex.split(str)[0]))
    

    def do_make_bubble(self, str):
        '''
        Wraps text is the corresponding argument is true, then pads text and sets inside a bubble.
        This is the text that appears above the cows

        make_bubble [-b brackets] [-d width] [-w wrap_text] message

        -b    Message brackets ('cowsay' or 'cowthink', default: 'cowsay')
        -d    Text bubble width (default: 40)
        -w    Whether text should be wrapped in the bubble ('true' or 'false' default: 't')
        '''
        params = {'-b': 'cowsay', '-d': 40, '-w': 'true'}
        s = shlex.split(str)
        for i in range(0, len(s)-1, 2):
            if s[i] in params:
                if s[i+1] in params:
                    print('Error: invalid syntax')
                    return
                params[s[i]] = s[i+1]
        if params['-w'] == 'true':
            wrapped = True
        if params['-w'] == 'false':
            wrapped = False
        print(cowsay.make_bubble(text=s[-1], brackets=cowsay.THOUGHT_OPTIONS[params['-b']], width=int(params['-d']), wrap_text=wrapped))


    def complete_make_bubble(self, text, line, begidx, endidx):
        completions = {
            '-b': ['cowsay', 'cowthink'],
            '-w': ['true', 'false']
        }
        if text != '':
            return [s for s in completions[shlex.split(line)[-2]] if s.startswith(text)]
        else:
            return [s for s in completions[shlex.split(line)[-1]]]


    def do_exit(self, str):
        print('Bye-bye!')
        return 1


if __name__ == '__main__':
    CowCommandLine().cmdloop()